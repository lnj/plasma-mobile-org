#!/usr/bin/env bash

bundle update
sed -i '1,/BUNDLED WITH/!d;s/BUNDLED WITH//g' Gemfile.lock
sed -i -e :a -e '/^\n*$/{$d;N;};/\n$/ba' Gemfile.lock
